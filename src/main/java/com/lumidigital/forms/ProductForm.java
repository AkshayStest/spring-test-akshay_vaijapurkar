package com.lumidigital.forms;

import java.math.BigDecimal;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;

public class ProductForm {

	@NotNull (message ="Please enter the name")
	@NotEmpty (message="Please enter the name")
	@Size(max=50, message="Name should be less than 50 characters")
	private String name;
	
	@NotNull (message="Please enter price")
	@Min(value=1, message="Please enter a valid amount")
	
	//@Min (message="Price not null")
	private BigDecimal price;
	
	@NotEmpty (message="Please select the currency")
	@NotNull (message="Please select the currency")
	private String currency;
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
