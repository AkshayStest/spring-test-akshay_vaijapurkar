package com.lumidigital.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String home(Model model) {
		model.addAttribute("exampleAttribute", "Luminescent Digital");
		return "home";
	}
	
	/*
	 * Task -2 
	 * This method sets the currency in Http Session
	 */
	@RequestMapping(value="/currency")
	public String getList(HttpServletRequest request, String currency){
	HttpSession session = request.getSession();
	session.setAttribute("sessionCurrency", currency);
	return "redirect:/product/list";
	}

}
