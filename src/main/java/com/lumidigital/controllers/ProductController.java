package src.main.java.com.lumidigital.controllers;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lumidigital.forms.ProductForm;
import com.lumidigital.model.Price;
import com.lumidigital.model.Product;
import com.lumidigital.service.ProductService;



@Controller
public class ProductController {

	private static final Logger LOG = Logger.getLogger(ProductController.class);
	@Resource(name="productService")
	private ProductService productService;
	
	private static final String REDIRECT_LIST_PAGE="productList";
	private static final String REDIRECT_EDIT_PAGE="editPage";
	private static final String VALIDATION_ERROR_LOG="Validation Error Occurred";
	/* 
	 * This method fetches all the Products 
	 * and returns to a Product List JSP
	 */ 
	@RequestMapping("/product/list")
	public String getList(Model model){
	
	Iterable<Product> prodList=productService.getProducts();
	model.addAttribute("prodList", prodList);
	return REDIRECT_LIST_PAGE;
	}
	
	/* 
	 * This method brings the Edit page
	 */ 
	@RequestMapping("/product/edit")
	public String getEditForm(Model model, String pid){
	
		ProductForm form=new ProductForm();
		form.setId(pid);
		model.addAttribute("productForm", form);
	return REDIRECT_EDIT_PAGE;
	}
	
	
	/* 
	 * This method captures the product form data and updates the Database
	 */ 
	
	@RequestMapping(value="/product/edit", method=RequestMethod.POST)
	public String submitEditForm(@Valid ProductForm productForm, BindingResult result,Map<String, Object> model){
		if(result.hasErrors())
	{
		LOG.error(VALIDATION_ERROR_LOG);	
		return REDIRECT_EDIT_PAGE;
	}

	LOG.info("Validation successful. Saving the product");
		
	try{	
	Product product=productService.getProductForId(new Long(productForm.getId()));
	product.setName(productForm.getName());
	Map<Currency,Price> prices=product.getPrices();
	
	prices.entrySet().stream().forEach(priceEntry->{
		if(priceEntry.getKey().getCurrencyCode().equals(productForm.getCurrency())){
			Price price= priceEntry.getValue();
			price.setAmount(productForm.getPrice());
			priceEntry.setValue(price);
		}
	});
	product.setPrices(prices);
	productService.save(product);
	}
	catch(Exception e)
	{
		LOG.error("ERROR DURING UPDAING THE PRODUCT :" +e.getMessage());
	}
	return "redirect:/product/list";
	}

	
	/* 
	 * Task 5 - 
	 * This method fetches all the Products 
	 * and returns a JSON response
	 */ 
	@ResponseBody
	@RequestMapping(value="/product/list", produces="application/json")
	public List<Product> getListInJson(Model model){
	Iterable<Product> prodIterable=productService.getProducts();
	List<Product> prodList=new ArrayList();
	prodIterable.forEach(prodList::add);
	return prodList;
	}

}
