<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<form:form method="POST" commandName="productForm" action="/product/edit">
Product Code : <form:input path="id"  readonly="true"  /><br/>
Product Name : <form:input path="name"  /> <form:errors path="name" cssStyle="color: red;" /><br/>
Product Price : <form:input path="price" /> <form:errors path="price" cssStyle="color: red;" /> <br/>
Currency : <form:select path="currency">
<form:options/>
<form:option value="">Select</form:option>
<form:option value="EUR">EUR</form:option>
<form:option value="GBP">GBP</form:option>

</form:select>

<form:errors path="currency" cssStyle="color: red;" />

<input type="submit" value="Save and go to List page"/>
</form:form>
