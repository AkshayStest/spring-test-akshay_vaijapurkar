<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html lang="en">


<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).on('change','#currencyDropDown',function(){
	window.location='/currency/?currency='+$('option:selected',this).text();
});
</script>

<h1>Product List page</h1>

<ul>
	<li><a href="/product/list">List products</a></li>
</ul>
<select id="currencyDropDown">
	<option value="">Select</option>
	<option <c:if test="${sessionCurrency eq 'EUR' }">selected=selected</c:if> value="EUR">EUR</option>
	<option <c:if test="${sessionCurrency eq 'GBP' }">selected=selected</c:if> value="GBP">GBP</option>
</select>
<br/>
<c:forEach var="product" items="${prodList}">

Product Code : ${product.id}<br/>
Product Name : ${product.description} <br/>
Product Description : ${product.description}<br/>
Price : 
<c:forEach var="price" items="${product.prices}">
<c:if test="${price.key eq sessionCurrency}">${price.value.amount}</c:if>
</c:forEach> 
<br/><br/>
<button type="button" onclick="javascript:window.location='/product/edit/?pid=${product.id}'" value="Edit">Edit</button><br/><br/>

</c:forEach>
</body>
</html>